import { createStore } from 'vuex'

export default createStore({
    state: {
        /* Set constants. */
        ONE_BITCOIN: 100000000,
        ONE_SMART_BITCOIN: 1000000000000000000,

        /* Initialize notifications. */
        notif: {
            isShowing: false,
            icon: null,
            title: null,
            description: null,
            delay: 7000,
        },

        /* Initialize network. */
        // NOTE: Current available options are `mainnet` and `testnet`.
        // network: null,
        network: 'testnet', // FOR DEV PURPOSES ONLY

        /* Initialize provider. */
        provider: null,

        /* Initialize block number. */
        blockNum: 0,

        /* USD price. */
        usd: null,
    },
    getters: {
        /**
         * (Blockchain Network) Provider
         *
         * Provides the currently active blockchain network provider.
         */
        getProvider(_state) {
            /* Validate network. */
            if (_state.network === 'mainnet') {
                // return 'https://smartbch.devops.cash/mainnet'
                return 'https://smartbch.fountainhead.cash/mainnet'
            } else if (_state.network === 'testnet') {
                // return 'https://smartbch.devops.cash/testnet'
                return 'https://moeing.tech:9545'
            }

            return null
        },

        /**
         * ABI
         *
         * Provides the ABI for the Campaign contract.
         */
        getAbi() {
            return require('../../contracts/CampaignV1.json')
        },

    },
    actions: {
        //
    },
    mutations: {
        //
    },
    modules: {
        //
    }
})
