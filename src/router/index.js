import { createRouter, createWebHashHistory } from 'vue-router'

import Help from '../views/Help'
import Home from '../views/Home'

const routes = [
    {
        path: '/',
        component: Home,
    },
    {
        path: '/help',
        component: Help,
    },
]

const router = createRouter({
    history: createWebHashHistory(process.env.BASE_URL),
    routes,
})

export default router
